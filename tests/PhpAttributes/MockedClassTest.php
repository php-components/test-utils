<?php

declare(strict_types=1);

namespace Tests\VarLab\TestUtils\PhpAttributes;

use PHPUnit\Framework\TestCase;
use Tests\VarLab\TestUtils\PhpAttributes\Mocks\ApiPlatformAttributeTestClass;
use VarLab\TestUtils\PhpAttributes\TestAttributesTrait;

class MockedClassTest extends TestCase
{
    use TestAttributesTrait;

    public function testAttributesWith(): void
    {
        $this->testClassAttributes();
        $this->testPropertyAttributes();

        $snapshotsFolder = __DIR__ . '/__snapshots__';
        self::assertFileExists($snapshotsFolder . '/MockedClassTest__testAttributes__1.json');
        self::assertFileExists($snapshotsFolder . '/MockedClassTest__testAttributes__2.json');
        self::assertFileExists($snapshotsFolder . '/MockedClassTest__testClassAttributes__1.json');
        self::assertFileExists($snapshotsFolder . '/MockedClassTest__testPropertyAttributes__1.json');
    }

    protected function getInstance(): object
    {
        return new ApiPlatformAttributeTestClass();
    }
}
