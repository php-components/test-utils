<?php

declare(strict_types=1);

namespace Tests\VarLab\TestUtils\PhpAttributes\Mocks;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Ignore;
use Symfony\Component\Uid\Ulid;

#[ApiResource(
    operations: [
        new Get(
            uriTemplate: '/test/{publicId}/prices',
            requirements: ['publicId' => '^([0123456789ABCDEFGHJKMNPQRSTVWXYZ]{26})$'],
            openapiContext: [
                'tags'        => ['Product'],
                'summary'     => 'Returns something.',
                'description' => 'This is a description.',
                'operationId' => 'something-to-test',
                'parameters'  => [
                    [
                        'schema'   => [
                            'type'    => 'string',
                            'example' => 'de_DE',
                            'default' => 'de_DE',
                        ],
                        'required' => true,
                        'pattern'  => '^[a-z]{2}_[A-Z]{2}$',
                    ],
                ],
                'responses'   => [
                    ['description' => 'Missing Header'],
                    ['description' => 'Not Found'],
                ],
            ],
        ),
    ],
    routePrefix: '/any-url',
    normalizationContext: [
        'groups' => ['group:read'],
    ],
)]
class ApiPlatformAttributeTestClass
{
    #[ApiProperty(identifier: false)]
    #[ORM\Id]
    #[ORM\Column(type: 'ulid', unique: true)]
    private Ulid $id;
    private Collection $aCollection;
    #[ORM\Column(length: 255, nullable: false)]
    private string $locale;
    #[ApiProperty(identifier: true)]
    #[ORM\Column(type: 'string', nullable: false)]
    private string $anotherId;

    #[ORM\Column(length: 255, unique: true)]
    #[Ignore]
    private string $anyString;

    public function getLocale(): string
    {
        return $this->locale;
    }

    public function setACollection(Collection $aCollection): void
    {
        $this->aCollection = $aCollection;
    }

    public function getACollection(): Collection
    {
        return $this->aCollection;
    }

    public function setAnotherId(string $anotherId): void
    {
        $this->anotherId = $anotherId;
    }

    public function getAnotherId(): string
    {
        return $this->anotherId;
    }

    public function setAnyString(string $anyString): void
    {
        $this->anyString = $anyString;
    }

    public function getAnyString(): string
    {
        return $this->anyString;
    }

    public function setId(Ulid $id): void
    {
        $this->id = $id;
    }

    public function getId(): Ulid
    {
        return $this->id;
    }

    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }
}
