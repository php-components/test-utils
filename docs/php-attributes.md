# Test PHP-Attributes

Since PHP 8, attributes can be used to add structured, machine-readable metadata information to declarations in code. 
They are used for Doctrine entities, for example.

In some cases, it is helpful for developers to be notified if an attribute has been changed and the tests (local or 
pipelined) should not turn green if a change has been made without checking the code again. This part of the test-utils 
solves that, by providing some traits that can be used in phpunit test case classes without writing much code.

It also works for Api-Platform attributes, which are used to configure API endpoints a description of the
Swagger UI to add or override the default. Since some of the attributes can override the default generated documentation
of the API, it is helpful to be informed of changes to avoid invalid documentation.

## How to use it

```php
<?php
# tests/Entity/EntityTest.php

class EntityTest extends TestCase
{
    use TestAttributesTrait;

    // ... some code to test the entity
    
    /**
    * This method is required. It must return an instance of the class that is tested with this test case.
    */
    protected function getInstance(): object
    {
        return new Entity();
    }

}
```

The trait uses [snapshot-tests](https://github.com/spatie/phpunit-snapshot-assertions) to check if the attributes has been changed.
