# Test-Utils for Symfony (and Api-Platform)

This package contains some utils for testing with phpunit:
* [Snapshot-Tests for PHP-Attributes](docs/php-attributes.md)

## Installation

The package is available on packagist.org and can be installed via composer:

```
composer require var-lab/test-utils:dev-main 
```

## Contribution

The source code is hosted on our GitLab-Servicer https://git.var-lab.com. Feel free to register an personal account
to contribute to our opensource projects.
